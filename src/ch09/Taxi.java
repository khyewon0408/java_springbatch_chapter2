package ch09;

public class Taxi {
	/*
	 * 20000원을 가지고 있었는데 10000원을 택시비로 사용했습니다.
		택시는 '잘나간다 운수' 회사 택시를 탔습니다.

	 * */
	String tcName;
	int passengerCount;
	int money;
	
	public Taxi(String tcName) {
		this.tcName =tcName;
	}
	
	public void take(int money) {  //승차
		this.money += money;
		passengerCount++;
	}
	
	public void showTaxiInfo() {
		System.out.println(tcName +" 수입은 " + money + "원 입니다");
	}
}
