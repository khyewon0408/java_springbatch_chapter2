package ch09;

public class TakeTransTest {

	public static void main(String[] args) {
		Student studentJ = new Student("James", 5000);
		Student studentT = new Student("Tomas", 10000);
		//추가 
		Student studentE = new Student("Edward", 20000);
		Bus bus100 = new Bus(100);
		
		Subway subwayGreen = new Subway(2);
		
		String tcNm ="잘나간다 운수";
		Taxi taxi =new Taxi(tcNm);
		
		studentJ.takeBus(bus100);
		studentT.takeSubway(subwayGreen);
		
		studentJ.showInfo();
		studentT.showInfo();
		
		bus100.showBusInfo();
		subwayGreen.showSubwayInfo();
		
		studentE.takeTaxi(taxi);
		studentE.showInfo();
		taxi.showTaxiInfo();

	}

}
