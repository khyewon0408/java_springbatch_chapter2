package ch12;

public class CarFactory {

	int carNo = 1000;
	
	private CarFactory() {}
	
	private static CarFactory instance =new CarFactory();
	
	public static CarFactory getInstance() {
		
		if(instance == null){
			instance = new CarFactory();
		}
		
		return instance;
	}
	
	public Car createCar() {
		
		Car car = new Car();
		return car ;
	}
}
